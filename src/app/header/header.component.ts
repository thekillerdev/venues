import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { NgForm } from '@angular/forms';
import _ from "lodash";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

    searchParams: string;

    constructor(private activatedRoute: ActivatedRoute, private router: Router) {
        // this.searchParams.subscribe((data) => {
        //     console.log('New data', data);
        // });
    }

    ngOnInit() {

    }

    onSubmit(form: NgForm) {
        let formQuery = _.get(form, 'value.q')
        if(!_.isEmpty(formQuery)) {
            this.router.navigate([formQuery]);
        }
    }

}
