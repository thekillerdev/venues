import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DataProviderService {

    _foursquareUrl: String;
    _fsClientId: String;
    _fsClientSecret: String;

    constructor(private http: HttpClient) {
        this._fsClientId = "B02FCM1WGTOAQSXQBAJQ4OMMYLTTJQSUXIHRRAAZ2GZEISVC";
        this._fsClientSecret = "3PTNTL2ZCMFEJIVX50MN3AAM0O3NBZ4WTC32KINQCKFI0UPL";
        this._foursquareUrl = "https://api.foursquare.com/v2/";
    }

    getUrlFor(path: String) {
        let today = new Date();
        let dd = ("0" + today.getDate()).slice(-2);
        let mm = ("0" + (today.getMonth() + 1)).slice(-2);
        let yyyy = today.getFullYear();
        let version = `${yyyy}${mm}${dd}`;
        return `${this._foursquareUrl}${path}&client_id=${this._fsClientId}&client_secret=${this._fsClientSecret}&v=${version}`;
    }

    getAllVenues() {
        return this.getVenuesByLocation("amsterdam")
    }

    getVenuesByLocation(location) {
        return this.http.get(this.getUrlFor(`venues/explore?near=${location}`))
    }

    getVenuesByLatLong(latitude, longitude) {
        return this.http.get(this.getUrlFor(`venues/explore?ll=${latitude},${longitude}`))
    }
}
