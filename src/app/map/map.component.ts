import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { } from '@types/googlemaps';

import { DataProviderService } from '../data-provider.service';
import { Observable } from 'rxjs';
import _ from "lodash";

import { ActivatedRoute } from "@angular/router";

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

    @ViewChild('gmap') _gmapElement: any;
    _map: google.maps.Map;
    _currentLat: number;
    _currentLong: number;
    _apiResponse: object;
    _currentMarkers: Array<google.maps.Marker>;
    _infoWindow: google.maps.InfoWindow;
    _geocoder: google.maps.Geocoder;

    constructor(private data: DataProviderService, private route: ActivatedRoute) {
        this._currentLat = 52.379189;
        this._currentLong = 4.899431;
        this._currentMarkers = [];
        this._infoWindow = new google.maps.InfoWindow();
        this._geocoder = new google.maps.Geocoder();

        this.route.params.subscribe(data => this.updateLocation(this, data));
    }

    ngOnInit() {
        var mapProp = {
            center: new google.maps.LatLng(this._currentLat, this._currentLong),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this._map = new google.maps.Map(this._gmapElement.nativeElement, mapProp);
    }

    findMe() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.centerPosition(position.coords.latitude, position.coords.longitude);
                this.getVenues();
            }, (error) => { console.log('Error: ', error) }, { timeout: 10000 });
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }

    centerPosition(lat, long) {
        this._currentLat = lat;
        this._currentLong = long;

        let location = new google.maps.LatLng(this._currentLat, this._currentLong);
        this._map.panTo(location);
    }

    formatVenueName(venueName, venueAddress) {
        return `${venueName}<br /><strong>Address:</strong> ${venueAddress}`;
    }

    addMarkers(venue) {
        let venueLatitude = _.get(venue, 'venue.location.lat');
        let venueLongitude = _.get(venue, 'venue.location.lng');
        let venueName = _.get(venue, 'venue.name')
        let venueAddress = _.get(venue, 'venue.location.address')
        if(venueLatitude && venueLongitude) {
            let location = new google.maps.LatLng(venueLatitude, venueLongitude);
            let marker = new google.maps.Marker({ position: location, map: this._map, title: this.formatVenueName(venueName, venueAddress) });
            this._currentMarkers.push(marker)

            marker.addListener('click', () => {
                this.markerHandler(marker);
            });
        }
    }

    removeMarkers() {
        if(this._currentMarkers.length > 0) {
            this._currentMarkers.forEach(function(marker){
                marker.setMap(null)
            })
        }
    }

    markerHandler(marker: google.maps.Marker) {
        this._infoWindow.setContent(marker.getTitle());
        this._infoWindow.setOptions({maxWidth: 200});
        this._infoWindow.open(this._map, marker);
    }

    getVenues() {
        this.data.getVenuesByLatLong(this._currentLat, this._currentLong).subscribe(
            data => this._apiResponse = this.filterVenues(data)
        );
    }

    getNearbyVenues(location) {
        this.data.getVenuesByLocation(location).subscribe(
            data => this._apiResponse = this.filterVenues(data)
        );
    }

    filterVenues(responseObject) {
        if(!responseObject || _.get(responseObject, 'meta.code') !== 200) { return {}; }
        let venues = _.get(responseObject, 'response.groups')
        let recommendedPlaces = _.find(venues, function (group) { return group.name === "recommended" });
        let that = this;

        if(recommendedPlaces.items.length > 0) {
            this.removeMarkers();
            recommendedPlaces.items.forEach(function(element){
                that.addMarkers(element)
            });
        }
    }

    updateLocation(that, locationProvided){
        if(locationProvided.searchQuery) {
            that._geocoder.geocode({'address': locationProvided.searchQuery}, function(results, status) {
                if (status === 'OK') {
                    that._currentLat = results[0].geometry.location.lat();
                    that._currentLong = results[0].geometry.location.lng();
                    that._map.setCenter(results[0].geometry.location);
                    that.getVenues()
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        } else {
            that.findMe();
            that.getVenues();
        }
    }
}
